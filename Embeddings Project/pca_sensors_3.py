# An example can be found here, the data load/format is pulled from this example. This example builds a Neural Network and does not use PCA.
# https://towardsdatascience.com/human-activity-recognition-har-tutorial-with-keras-and-core-ml-part-1-8c05e365dfa0
#
# This program uses the functions to build the base for data retreival, PCA and other additions are not part of the example above. 
from __future__ import absolute_import, division, print_function, unicode_literals
import tensorflow as tf
from tensorflow import keras
from sklearn.decomposition import PCA
from sklearn import preprocessing
from itertools import cycle
from itertools import chain
from scipy import stats
import numpy as np
import pandas as pd
import seaborn as sns

import matplotlib.pyplot as plt
import re
import math

# Globals
pd.options.display.float_format = '{:.1f}'.format
sns.set()
plt.style.use('ggplot')
print('Keras Version', keras.__version__)

LABELS = ['Downstairs', 'Jogging', 'Sitting', 'Standing', 'Upstairs', 'Walking']
TIME_PERIOD = 80
WINDOW = 40
# originally used to fine tune the values, but that is now being done in a loop with several different dimension sizes.
tuner = 1

def read_data(file_path):
        column_names = ['user-id','activity','timestamp','x-axis','y-axis','z-axis']
        df = pd.read_csv(file_path, header=None, names=column_names)

        df['z-axis'].replace(regex=True,inplace=True,to_replace=r';',value=r'')
        df['z-axis'] = df['z-axis'].apply(convert_to_float)

        df.dropna(axis=0,how='any',inplace=True)
        
        return df

def convert_to_float(x):
    try:
        return np.float(x)
    except:
        return np.nan

def show_basic_dataframe(dataframe):
    print('Number of columns in the dataframe: %i' % (dataframe.shape[1]))
    print('Number of rows in the dataframe: %i' % (dataframe.shape[0]))

def plot_activity(activity, data):
    fig, (ax0, ax1, ax2) = plt.subplots(nrows=3, figsize=(15,10), sharex=True)

    plot_axis(ax0, data['timestamp'], data['x-axis'], 'X-Axis')
    plot_axis(ax1, data['timestamp'], data['y-axis'], 'Y-Axis')
    plot_axis(ax2, data['timestamp'], data['z-axis'], 'Z-Axis')
    plt.subplots_adjust(hspace=0.2)
    fig.suptitle(activity)
    plt.subplots_adjust(top=0.90)
    plt.show()

def plot_axis(ax,x,y,title):
    ax.plot(x,y,'r')
    ax.set_title(title)
    ax.xaxis.set_visible(False)
    ax.set_ylim([min(y)-np.std(y), max(y) + np.std(y)])
    ax.set_xlim([min(x), max(x)])
    ax.grid(True)

def create_segments_and_labels(df, time_steps, step, label_names):
    partitions = 3

    segments = []
    labels = []

    for i in range(0,len(df) - time_steps, step):
        xs = df['x-axis'].values[i: i + time_steps]
        ys = df['y-axis'].values[i: i + time_steps]
        zs = df['z-axis'].values[i: i + time_steps]

        label = stats.mode(df[label_names][i: i + time_steps])[0][0]
        segments.append([xs, ys, zs])
        labels.append(label)

    reshaped_segments = np.asarray(segments, dtype=np.float32).reshape(-1, time_steps, partitions)
    labels = np.asarray(labels)

    return reshaped_segments, labels

if __name__ == '__main__':
    df = read_data('WISDM_ar_v1.1/WISDM_ar_v1.1_raw.txt')
    show_basic_dataframe(df)
    df.head(20)

    df['activity'].value_counts().plot(kind='bar', title='Training Examples by Activity Type')
    plt.show()

    df['user-id'].value_counts().plot(kind='bar',title='Training Examples by User')
    plt.show()

    
    for activity in np.unique(df['activity']):
        subset = df[df['activity'] == activity][:180]
        plot_activity(activity,subset)
    
    LABEL = 'ActivityEncoded'
    le = preprocessing.LabelEncoder()

    df[LABEL] = le.fit_transform(df['activity'].values.ravel())

    # Separate based of user id.
    df_test = df[df['user-id'] > 28 ]
    df_train = df[df['user-id'] <= 28 ]
    df_train = df

    pd.options.mode.chained_assignment = None
    df_train['x-axis'] = df_train['x-axis'] / df_train['x-axis'].max()
    df_train['y-axis'] = df_train['y-axis'] / df_train['y-axis'].max()
    df_train['z-axis'] = df_train['z-axis'] / df_train['z-axis'].max()

    df_train = df_train.round({'x-axis': 4, 'y-axis':4, 'z-axis': 4})

    X_train, y_train = create_segments_and_labels(df_train, TIME_PERIOD, WINDOW, LABEL)

    print('X_train Shape:', X_train.shape)
    print(X_train.shape[0], 'training samples')
    print('y_train shape: ', y_train.shape)

    samps, secs, accel = X_train.shape
    red_dim = secs * accel
    print("Features: %d" % red_dim )
    # Multiple PCA
    components = [ 2, 5, 10, 20, 40, 80, 160, red_dim ]
    accuracy = []
    for j in components:
        #pca = PCA(n_components=red_dim-tuner)
        pca = PCA(n_components=j)
        
        X_train2d = X_train.reshape(-1, red_dim)
        # Use entire dataset.
        ds_pca = pca.fit_transform(X_train2d)
        ds_rec = pca.inverse_transform(ds_pca)

        colors = ['b','g','r','c','m','y','k']
        markers = ['+','o','^','v','<','>','D','h','s']

        for i, c, m in zip(np.unique(y_train), cycle(colors), cycle(markers)):
            plt.scatter(ds_pca[y_train==i, 0], ds_pca[y_train == i, 1],
                c=c, marker=m, label=i, alpha=0.5)
            _ = plt.legend(loc='best')

        plt.title("PCA Dims: %d" % j)
        plt.show()